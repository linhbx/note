
### **Bước 1: Cài đặt thư viện BLOB Storing**  
Trong project của ứng dụng ABP Framework, cài đặt thư viện BLOB Storing bằng cách chạy lệnh sau trong Package Manager Console:
```
Install-Package Volo.Abp.BlobStoring
```
### **Bước 2: Cấu hình BLOB Storing**  
Trong file `appsettings.json`, thêm cấu hình BLOB Storing:

```
"BlobStoring": {
  "DefaultProvider": "FileSystem",
  "FileSystem": {
    "BasePath": "C:/Temp/Uploads"
  }
}
```
Trong ví dụ trên, chúng ta sử dụng `FileSystem` provider để lưu trữ file. Bạn có thể thay đổi cấu hình này để sử dụng các provider khác như Azure Blob Storage, Amazon S3, etc.

### **Bước 3: Tạo Service để upload và download file**  
Trong project Application layer, tạo một service để upload và download file:
```
public class FileAppService : ApplicationService, IFileAppService
{
    private readonly IBlobContainer<FileContainer> _blobContainer;

    public FileAppService(IBlobContainer<FileContainer> blobContainer)
    {
        _blobContainer = blobContainer;
    }

    public async Task<List<string>> UploadFilesAsync(List<UploadFileInput> inputs)
    {
        var blobNames = new List<string>();
        foreach (var input in inputs)
        {
            using (var stream = new MemoryStream(input.File))
            {
                var blobName = Guid.NewGuid().ToString() + Path.GetExtension(input.FileName);
                await _blobContainer.SaveAsync(blobName, stream);
                blobNames.Add(blobName);
            }
        }
        return blobNames;
    }

    public async Task<FileDto> GetFileAsync(string blobName)
    {
        var stream = await _blobContainer.GetAsync(blobName);
        return new FileDto(stream.ToArray(), blobName);
    }
}
```

Trong đoạn code trên, phương thức `UploadFilesAsync` sẽ upload nhiều file và trả về danh sách tên file đã được lưu. Phương thức `GetFileAsync` vẫn tương tự như hướng dẫn trước, để tải file từ BLOB Storing.

### **Bước 4: Tích hợp với Angular**  
Trong Angular application, tạo một service để gọi API upload và download file:
```
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  constructor(private http: HttpClient) {}

  uploadFiles(files: File[]): Promise<string[]> {
    const formData = new FormData();
    files.forEach((file, index) => {
      formData.append(`files[${index}].file`, file);
      formData.append(`files[${index}].fileName`, file.name);
    });

    return this.http.post<string[]>('/api/app/file/upload', formData).toPromise();
  }

  downloadFile(blobName: string): Promise<Blob> {
    return this.http.get(`/api/app/file/download?blobName=${blobName}`, { responseType: 'blob' }).toPromise();
  }
}
```

Trong component của Angular, gọi service này để upload và download file:
```
import { FileService } from './file.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent {
  constructor(private fileService: FileService) {}

  onFilesSelected(event: any) {
    const files: File[] = Array.from(event.target.files);
    this.fileService.uploadFiles(files).then((blobNames) => {
      // Handle successful upload
    });
  }

  downloadFile(blobName: string) {
    this.fileService.downloadFile(blobName).then((blob) => {
      // Handle downloaded file
    });
  }
}
```

Với các bước hướng dẫn trên, bạn có thể triển khai chức năng upload multi files và download file trong ứng dụng ABP Framework sử dụng .NET Core 8, Angular và MongoDB bằng thư viện BLOB Storing. Lưu ý rằng trong ví dụ này, chúng ta sử dụng danh sách tên file để quản lý các file đã được upload, nhưng bạn có thể lưu trữ thông tin về các file này vào cơ sở dữ liệu MongoDB nếu cần.

## **Giải thích chi tiết hơn về cách truyền tham số giữa frontend và backend khi triển khai chức năng upload multi files và download file trong ứng dụng ABP Framework.**

**Backend (C#)**  
Trong `FileAppService`, chúng ta sẽ định nghĩa một DTO để nhận dữ liệu từ frontend:

```
public class UploadFileInput
{
    public string FileName { get; set; }
    public byte[] File { get; set; }
}
```

Phương thức `UploadFilesAsync` sẽ nhận một danh sách các `UploadFileInput`:

```
public async Task<List<string>> UploadFilesAsync(List<UploadFileInput> inputs)
{
    var blobNames = new List<string>();
    foreach (var input in inputs)
    {
        using (var stream = new MemoryStream(input.File))
        {
            var blobName = Guid.NewGuid().ToString() + Path.GetExtension(input.FileName);
            await _blobContainer.SaveAsync(blobName, stream);
            blobNames.Add(blobName);
        }
    }
    return blobNames;
}
```

**Frontend (Angular)**  
Trong Angular service, chúng ta sẽ định nghĩa một interface để biểu diễn một file:

```
export interface UploadFileInput {
  fileName: string;
  file: File;
}
```

Phương thức `uploadFiles` sẽ nhận một mảng các `UploadFileInput`:

```
uploadFiles(files: UploadFileInput[]): Promise<string[]> {
  const formData = new FormData();
  files.forEach((file, index) => {
    formData.append(`files[${index}].fileName`, file.fileName);
    formData.append(`files[${index}].file`, file.file);
  });

  return this.http.post<string[]>('/api/app/file/upload', formData).toPromise();
}
```

Trong component của Angular, chúng ta sẽ tạo một mảng các `UploadFileInput` từ danh sách các file được chọn:

```
onFilesSelected(event: any) {
  const files: File[] = Array.from(event.target.files);
  const uploadFiles: UploadFileInput[] = files.map((file) => ({
    fileName: file.name,
    file: file
  }));
  this.fileService.uploadFiles(uploadFiles).then((blobNames) => {
    // Handle successful upload
  });
}
```

Như vậy, khi frontend gửi request lên backend, nó sẽ gửi một mảng các `UploadFileInput`, mỗi phần tử trong mảng sẽ chứa tên file và dữ liệu file. Backend sẽ nhận và xử lý danh sách này, sau đó trả về một danh sách tên file đã được lưu trữ.

Khi download file, frontend sẽ gửi tên file (blobName) lên backend, và backend sẽ trả về file dưới dạng `Blob`.